#!/bin/sh
SERVER_IS_RUNNING=$(symfony server:status | grep 'Not Running')
echo "$SERVER_IS_RUNNING"
if [ ! "$SERVER_IS_RUNNING" ]; then
  symfony server:stop
fi
symfony serve